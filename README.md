## Benepass Employee UI Clone

This project is a clone of Benepass's Employee UI, as part of the frontend job application.

**Open the [Demo](https://benepass-clone-gabriel.vercel.app/) to see the result.**

![](docs/screenshots/home-web.png)
![](docs/screenshots/explore-web.png)

|                 Home                  |                 Menu                  | Explore                                  |
| :-----------------------------------: | :-----------------------------------: | ---------------------------------------- |
| ![](docs/screenshots/home-mobile.jpg) | ![](docs/screenshots/menu-mobile.jpg) | ![](docs/screenshots/explore-mobile.jpg) |

## Run locally

```bash
npm run dev
# or
yarn dev
```

## Features

- Replicates Home and Explore pages of the Employee Dashboard UI.
- Responsive for mobile devices.
- Filter merchants by name on Explore page using the Search Bar.

## 🛠 Tech and Libraries

- ⚛️ [React](https://reactnative.dev/) for building interactive user interfaces.
- 🔼 [Next.js](https://nextjs.org/) for server-side rendering and routing.
- 🔵 [TypeScript](https://www.typescriptlang.org/) for type safety and improved development experience.
- 🎨 [Tailwind CSS](https://tailwindcss.com/) for styling and responsive design.

## Author

<a href="https://www.linkedin.com/in/gabriel-v-rodrigues/">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/57915410?s=400&u=cdd796470a4e976ddb0580172f12337777b99dcf&v=4" width="100px;" alt=""/>
 <br />
 <sub><b>Gabriel Rodrigues</b></sub></a> <a href="https://www.linkedin.com/in/gabriel-v-rodrigues/">🚀</a>

<sup>Made with ❤️ by Gabriel Rodrigues<sup>
