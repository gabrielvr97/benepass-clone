"use client";

import { SearchBar } from "@/components/SearchBar/SearchBar";
import { ItemCard } from "@/components/templates/ItemCard";
import { Merchant } from "@/models/Merchant";
import { getMerchants } from "@/services/merchantService";
import { XMarkIcon } from "@heroicons/react/24/solid";
import { useState } from "react";

export default function Explore() {
  const merchantsByCategory = getMerchants();
  const [searchMode, setSearchMode] = useState(false);
  const [merchantsFiltered, setMerchantsFiltered] = useState<Merchant[]>([]);

  const onSetSearchTerm = (searchTerm: string) => {
    const isSearching = !!searchTerm;
    setSearchMode(isSearching);

    if (isSearching) {
      setMerchantsFiltered(
        merchantsByCategory
          .map((merchantCategory) => merchantCategory.merchants)
          .flat(1)
          ?.filter((merchant) =>
            merchant.name.toLowerCase().includes(searchTerm.toLowerCase())
          )
      );
    }
  };

  return (
    <div className="md:w-[80%] w-full md:ml-[20%] md:p-14 p-8">
      <div className="flex flex-col md:flex-row justify-between md:items-start gap-4 my-12">
        <div>
          <h1 className="font-bold text-4xl text-primary">Explore</h1>
          <h4 className="text-gray-600">
            Find popular vendors where you can use your benefits
          </h4>
        </div>
        <SearchBar onSetSearchTerm={onSetSearchTerm} />
      </div>

      {!searchMode ? (
        merchantsByCategory.map((merchantCategory, index) => (
          <div className="my-4" key={`${merchantCategory.title}-${index}`}>
            <h2 className=" text-primary text-lg font-semibold">
              {merchantCategory.title}
            </h2>
            <div className="overflow-x-auto whitespace-nowrap py-4">
              {merchantCategory.merchants.map((merchant, index) => (
                <div
                  className="flex justify-center md:inline-block md:mx-4 my-6"
                  key={`${merchant.name}-${index}`}
                >
                  <ItemCard
                    title={merchant.name}
                    imageUrl={merchant.imageUrl}
                    description={merchant.description}
                    category={merchant.category}
                  />
                </div>
              ))}
            </div>
          </div>
        ))
      ) : (
        <div className="flex flex-row flex-wrap justify-center">
          {!merchantsFiltered?.length && (
            <div className="flex flex-row text-primary items-center gap-2">
              <XMarkIcon width={32} height={32} />
              <p className="text-lg ">No vendors were found</p>
            </div>
          )}
          {merchantsFiltered.map((merchant, index) => (
            <div className="m-4 py-4" key={`${merchant.name}-${index}`}>
              <ItemCard
                title={merchant.name}
                imageUrl={merchant.imageUrl}
                description={merchant.description}
                category={merchant.category}
              />
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
