import { HeaderMobile } from "@/components/HeaderMobile/HeaderMobile";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { SideMenu } from "../components/SideMenu/SideMenu";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Benepass",
  description: "Benepass clone by Gabriel Rodrigues",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`${inter.className} flex flex-row`}>
        <HeaderMobile />
        <SideMenu />
        {children}
      </body>
    </html>
  );
}
