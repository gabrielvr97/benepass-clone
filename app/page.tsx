import WalletIllustration from "@/assets/wallet.png";
import { SignedInUser } from "@/components/SideMenu/SignedInUser/SignedInUser";
import Image from "next/image";
import { RecentTransactions } from "../components/Home/RecentTransations";
import { Updates } from "../components/Home/Updates";

export default function Home() {
  return (
    <main className="flex flex-col w-full bg-background md:ml-[20%]">
      <div
        className={`w-full h-24 hidden md:flex flex-row justify-between md:items-start items-center gap-2 px-4 md:p-14 `}
      >
        <div>
          <h1 className="font-bold text-xl md:text-4xl text-primary">
            Welcome back!
          </h1>
          <h4 className="text-gray-600">Lauren Raub</h4>
        </div>
        <SignedInUser />
      </div>

      <div className="md:p-14 p-8">
        <div className="w-full bg-primary h-40 rounded-md flex flex-row items-center relative mt-24 md:mt-4">
          <div className="flex flex-col gap-2 pl-6">
            <p className="text-white">You have</p>
            <p className="text-white md:text-6xl text-3xl font-bold">$698.43</p>
            <p className="text-white">Across 6 different benefits</p>
          </div>
          <Image
            src={WalletIllustration}
            width={96}
            height={96}
            alt="Wallet illustration"
            className="absolute -bottom-1 -right-1 md:w-24 w-16"
          />
        </div>

        <div className="flex md:flex-row flex-col gap-6">
          <RecentTransactions />
          <Updates />
        </div>
      </div>
    </main>
  );
}
