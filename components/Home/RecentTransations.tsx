import { getRecentTransactions } from "@/services/transactionService";
import { ArrowRightIcon } from "@heroicons/react/24/solid";
import Link from "next/link";
import { TransactionItem } from "../Transactions/TransactionItem";

export function RecentTransactions() {
  const recentTransactions = getRecentTransactions();

  return (
    <div className="flex flex-col mt-10 gap-4 md:w-2/3">
      <div className="flex flex-row justify-between">
        <h1 className="text-gray-600">Recent Transactions</h1>
        <Link href={"/"} className="flex flex-row gap-2 items-center">
          <p className="underline">View all</p>{" "}
          <ArrowRightIcon width={16} height={16} />
        </Link>
      </div>

      {recentTransactions.map((transaction) => (
        <TransactionItem key={transaction.id} transaction={transaction} />
      ))}
    </div>
  );
}
