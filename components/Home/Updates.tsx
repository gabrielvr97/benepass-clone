import { getUpdates } from "@/services/updateServices";
import { InfoCard } from "../templates/InfoCard";

export function Updates() {
  const updates = getUpdates();
  return (
    <div className="flex flex-col md:w-1/3 mt-10 gap-4">
      <h1>Updates</h1>
      <div className="bg-gray-200 flex flex-col gap-4 p-6 rounded-md">
        {updates.map((update, index) => (
          <InfoCard
            key={`${update.title}-${index}`}
            title={update.title}
            description={update.description}
            icon={update.icon}
            color={update.color}
          />
        ))}
      </div>
    </div>
  );
}
