"use client";

import { MagnifyingGlassIcon, XCircleIcon } from "@heroicons/react/24/solid";
import { useEffect, useState } from "react";
import { useDebounce } from "use-debounce";

type SearchBarProps = {
  onSetSearchTerm: (searchTerm: string) => void;
};
export function SearchBar({ onSetSearchTerm }: SearchBarProps) {
  const [searchTerm, setSearchTerm] = useState("");
  const [searchTermDebouced] = useDebounce(searchTerm, 800);

  const handleClearSearchTerm = () => {
    setSearchTerm("");
  };

  useEffect(() => {
    onSetSearchTerm(searchTermDebouced);
  }, [searchTermDebouced]);

  return (
    <div className="flex flex-row items-center gap-3">
      <MagnifyingGlassIcon width={24} height={24} className="text-primary" />
      <input
        className="border-2 py-2 px-4 rounded-md md:w-72 w-full"
        placeholder="Search a vendor"
        onChange={(e) => setSearchTerm(e.target.value)}
        value={searchTerm}
      />
      <XCircleIcon
        className={`text-primary cursor-pointer scale-0 transition-all ${
          searchTerm && "scale-100"
        }`}
        width={24}
        height={24}
        onClick={handleClearSearchTerm}
      />
    </div>
  );
}
