"use client";

import logo from "@/assets/benepass-logo.png";
import Image from "next/image";
import Link from "next/link";
import { MenuItem } from "./MenuItem/MenuItem";

import { getUser } from "@/services/userService";
import {
  BanknotesIcon,
  BuildingStorefrontIcon,
  Cog6ToothIcon,
  CurrencyDollarIcon,
  DocumentChartBarIcon,
  HomeIcon,
  QuestionMarkCircleIcon,
  XMarkIcon,
} from "@heroicons/react/24/solid";
import { usePathname } from "next/navigation";
import { useEffect } from "react";
import { Button } from "../templates/Button";

type SideMenuProps = {
  visible?: boolean;
  onCloseMenu?: () => void;
};
export function SideMenu({ visible, onCloseMenu }: SideMenuProps) {
  const pathname = usePathname();
  const user = getUser();

  const menuItems: IMenuItem[] = [
    { url: "/", label: "Home", icon: <HomeIcon width={24} height={24} /> },
    {
      url: "/explore",
      label: "Explore",
      icon: <BuildingStorefrontIcon width={24} height={24} />,
    },
    {
      url: "",
      label: "Transactions",
      icon: <BanknotesIcon width={24} height={24} />,
    },
    {
      url: "",
      label: "Documents",
      icon: <DocumentChartBarIcon width={24} height={24} />,
    },
  ];

  const menuItemsFooter: IMenuItem[] = [
    {
      url: "",
      label: "Contact Support",
      icon: <QuestionMarkCircleIcon width={24} height={24} />,
    },
    {
      url: "",
      label: "Settings",
      icon: <Cog6ToothIcon width={24} height={24} />,
    },
  ];

  useEffect(() => {
    if (visible && onCloseMenu) {
      onCloseMenu();
    }
  }, [pathname]);

  return (
    <aside
      className={`bg-primary ${
        visible ? "flex translate-y-0" : "translate-y-full md:translate-y-0"
      } md:w-1/5 w-full z-30 h-screen fixed p-4 py-6 drop-shadow-sm md:flex flex-col justify-between transition-all duration-500 transform md:transition-none`}
    >
      <div className="flex flex-row items-center justify-between">
        <button
          className="md:hidden"
          onClick={() => onCloseMenu && onCloseMenu()}
        >
          <XMarkIcon className=" text-white" width={40} height={40} />
        </button>
        <p className="md:hidden text-white">{user.name}</p>
        <Link href={"/"} className="w-12 h-12 my-5">
          <Image src={logo} height={48} width={48} alt="Benepass Logo" />
        </Link>
      </div>

      <ul className="flex flex-col gap-4 md:h-3/4 h-1/2">
        {menuItems.map((menuItem, index) => (
          <MenuItem
            key={`${menuItem.label}-${index}`}
            menuItem={menuItem}
            selected={pathname === menuItem.url}
          />
        ))}
      </ul>

      <Button className="my-5">
        <CurrencyDollarIcon width={24} height={24} className="text-primary" />
        <p className="text-primary">Get reimbursed</p>
      </Button>

      <div className="border-t-[1px] border-t-gray-600" />

      <ul className="flex flex-col gap-4 mt-4">
        {menuItemsFooter.map((menuItem, index) => (
          <MenuItem
            key={`${menuItem.label}-${index}`}
            menuItem={menuItem}
            selected={false}
          />
        ))}
      </ul>
    </aside>
  );
}
