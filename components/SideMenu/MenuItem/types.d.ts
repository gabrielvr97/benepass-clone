interface IMenuItem {
  url: string;
  label: string;
  icon: React.SVGProps<SVGSVGElement>;
}
