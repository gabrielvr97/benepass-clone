import Link from "next/link";

type MenuItemProps = {
  menuItem: IMenuItem;
  selected: boolean;
};

export function MenuItem({ menuItem, selected }: MenuItemProps) {
  return (
    <li>
      <Link
        href={menuItem.url}
        className={`p-4 rounded-lg flex flex-row gap-4
        ${
          selected
            ? "bg-white text-primary"
            : "text-white hover:bg-white hover:text-primary"
        }
      `}
      >
        {menuItem.icon}
        {menuItem.label}
      </Link>
    </li>
  );
}
