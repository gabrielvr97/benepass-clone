import { getUser } from "@/services/userService";
import { ChevronDownIcon } from "@heroicons/react/24/solid";

export function SignedInUser() {
  const user = getUser();

  const getUsernameInitials = (name: string) => {
    if (name) {
      const nameParts = name.split(" ");
      const firstInitial = nameParts[0][0].toUpperCase();
      const lastInitial = nameParts[nameParts.length - 1][0];

      return `${firstInitial}${lastInitial}`;
    }
    return "U";
  };

  return (
    <div className="flex flex-row items-center justify-between gap-2 cursor-pointer">
      <div className="rounded-full bg-primary w-10 h-10 md:w-14 md:h-14 flex justify-center items-center">
        <p className="text-white">{getUsernameInitials(user.name)}</p>
      </div>
      <ChevronDownIcon
        width={24}
        height={24}
        className="text-primary md:w-6 w-5"
      />
    </div>
  );
}
