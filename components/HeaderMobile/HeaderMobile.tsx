"use client";

import logo from "@/assets/benepass-logo-blue.png";
import { Bars3Icon } from "@heroicons/react/24/solid";
import Image from "next/image";
import { useState } from "react";
import { SideMenu } from "../SideMenu/SideMenu";
import { SignedInUser } from "../SideMenu/SignedInUser/SignedInUser";

export function HeaderMobile() {
  const [menuVisible, setMenuVisible] = useState(false);

  return (
    <>
      <div
        className={`w-full h-20 md:hidden flex flex-row justify-between items-center mb-5 gap-2 fixed z-10 bg-white px-4`}
      >
        <Bars3Icon
          width={32}
          height={32}
          className="text-primary"
          onClick={() => setMenuVisible(true)}
        />
        <Image src={logo} width={48} height={48} alt="Blue Benepass logo" />
        <SignedInUser />
      </div>
      <SideMenu
        visible={menuVisible}
        onCloseMenu={() => setMenuVisible(false)}
      />
    </>
  );
}
