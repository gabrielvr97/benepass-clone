import { Transaction, TransactionStatus } from "@/models/Transaction";
import {
  CheckCircleIcon,
  ClockIcon,
  ExclamationTriangleIcon,
  XCircleIcon,
} from "@heroicons/react/24/solid";
import Tooltip from "../templates/Tooltip";

interface TransactionItemProps {
  transaction: Transaction;
}
export function TransactionItem({ transaction }: TransactionItemProps) {
  const statusIcon: {
    [STATUS in TransactionStatus]: React.SVGProps<SVGSVGElement>;
  } = {
    CONFIRMED: (
      <CheckCircleIcon
        width={32}
        height={32}
        className="text-green-600 md:w-8 w-4"
      />
    ),
    PENDING: (
      <ClockIcon width={32} height={32} className="text-blue-600 md:w-8 w-4" />
    ),
    ACTION_REQUIRED: (
      <ExclamationTriangleIcon
        width={32}
        height={32}
        className="text-yellow-600 md:w-8 w-4"
      />
    ),
    CANCELED: (
      <XCircleIcon width={32} height={32} className="text-red-600 md:w-8 w-4" />
    ),
  };

  const formattedStatus = (status: string): string => {
    const statusParts = status.toLowerCase().split("_");
    return statusParts
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(" ");
  };

  return (
    <div className="bg-white md:p-5 p-3 rounded-md flex flex-row justify-between drop-shadow-sm">
      <div className="flex flex-row items-center md:gap-6 gap-4">
        <p className="text-gray-500 text-xs md:text-base">{transaction.date}</p>
        <div className="flex flex-col md:flex-row gap-1 text-sm md:text-base text-primary">
          <p>{transaction.establishment}</p>
          <p className="hidden md:block">•</p>
          <p>{transaction.category}</p>
        </div>
      </div>
      <div className="flex flex-row md:gap-6 gap-2 items-center">
        <p
          className={`text-sm md:text-xl font-semibold ${
            transaction.status === "CANCELED" && "line-through"
          }`}
        >
          - ${transaction.amount.toFixed(2)}
        </p>
        <Tooltip message={formattedStatus(transaction.status)}>
          {statusIcon[transaction.status]}
        </Tooltip>
      </div>
    </div>
  );
}
