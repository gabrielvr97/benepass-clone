import {
  BanknotesIcon,
  CreditCardIcon,
  ExclamationTriangleIcon,
  GiftIcon,
  NewspaperIcon,
} from "@heroicons/react/24/solid";

type CardProps = {
  title: string;
  description: string;
  color: string;
  icon: string;
};

export function InfoCard({ title, description, color, icon }: CardProps) {
  const bgColors: { [key: string]: string } = {
    green: "bg-green-100",
    yellow: "bg-yellow-100",
    blu: "bg-blue-100",
    default: "bg-slate-200",
  };

  const iconsColors: { [key: string]: string } = {
    green: "text-green-700",
    yellow: "text-yellow-700",
    blue: "text-blue-700",
    default: "text-gray-700",
  };

  const icons: { [key: string]: React.SVGProps<SVGSVGElement> } = {
    warning: (
      <ExclamationTriangleIcon className={iconsColors[color]} width={32} />
    ),
    card: <CreditCardIcon className={iconsColors[color]} width={32} />,
    bank: <BanknotesIcon className={iconsColors[color]} width={32} />,
    party: <GiftIcon className={iconsColors[color]} width={32} />,
    default: <NewspaperIcon className={iconsColors["default"]} width={32} />,
  };

  return (
    <div className="bg-white p-6 rounded-md flex flex-row items-center gap-4">
      <div
        className={`w-16 h-16 rounded-md flex items-center justify-center ${
          bgColors[color] || bgColors["default"]
        }`}
      >
        {icons[icon] || icons["default"]}
      </div>
      <div className="flex-1">
        <h2 className="text-lg font-semibold">{title}</h2>
        <p className="text-sm text-gray-600">{description}</p>
      </div>
    </div>
  );
}
