interface TooltipProps {
  message: string;
  children: React.ReactNode;
}
export default function Tooltip({ message, children }: TooltipProps) {
  return (
    <div className="group relative flex">
      {children}
      <p className="absolute bottom-10 scale-0 transition-all rounded bg-gray-800 p-2 text-xs text-white group-hover:scale-100 whitespace-nowrap">
        {message}
      </p>
    </div>
  );
}
