import { ReactNode } from "react";

type ButtonProps = {
  children: ReactNode;
  className?: string;
};

export function Button({ children, className = "" }: ButtonProps) {
  return (
    <button
      className={`bg-white py-3 rounded-md flex flex-row justify-center items-center gap-2 hover:opacity-80
        ${className}`}
    >
      {children}
    </button>
  );
}
