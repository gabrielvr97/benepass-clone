import Image from "next/image";

type ItemCardProps = {
  title: string;
  category: string;
  description: string;
  imageUrl: string;
};
export function ItemCard({
  title,
  category,
  description,
  imageUrl,
}: ItemCardProps) {
  return (
    <div className="bg-white h-72 w-64 drop-shadow-md rounded-md cursor-pointer hover:drop-shadow-xl">
      <Image
        src={imageUrl}
        width={250}
        height={250}
        className="object-cover w-full h-32 rounded-t-md"
        alt="Merchant Logo"
      />
      <div className="flex flex-col p-5 gap-1 w-full">
        <p className="text-gray-400 text-sm font-semibold">
          {category.toUpperCase()}
        </p>
        <h1 className="text-primary text-lg font-semibold text-wrap">
          {title}
        </h1>
        <p className="text-gray-600 text-sm text-wrap line-clamp-3">
          {description}
        </p>
      </div>
    </div>
  );
}
