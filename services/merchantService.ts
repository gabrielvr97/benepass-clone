import { MerchantCategory } from "@/models/Merchant";

export const getMerchants = (): MerchantCategory[] => {
  const merchantsCategories: MerchantCategory[] = [
    {
      title: "Cooworking",
      merchants: [
        {
          name: "WeWork",
          description:
            "Whether you're an established enterprise or a scaling startup, find an office space that's right for you.",
          imageUrl:
            "https://cdn6.aptoide.com/imgs/5/4/5/54545aaf5b5c3f0275162477421727b7_icon.png",
          category: "Work From Home",
        },
        {
          name: "X Kom",
          description: "Electronic shop in Katowice (DH Supersam)",
          imageUrl:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwHmuosyc86lnoEcs87QCdRd414OCgvdKSttXThVTkH9l_prPleYEhAXu1uL3azoIOM-Q&usqp=CAU",
          category: "Tech Accessories",
        },
        {
          name: "Verizon High Speed Internet",
          description: "Experience Verizon's 100% fiber optic internet service",
          imageUrl:
            "https://i.pinimg.com/1200x/19/24/82/19248216dafbf911bf1d6c1f666e0aee.jpg",
          category: "Internet",
        },
        {
          name: "Vari",
          description:
            "Shop standing desks by Vari. Our high-quality height-adjustable standing desks work great in any office. Easy Assembly.",
          imageUrl:
            "https://media.licdn.com/dms/image/C4E0BAQEHvKBNK3bJ8Q/company-logo_200_200/0/1641325810828/vari_logo?e=2147483647&v=beta&t=XVYr3ndRzvH0a9Ww3WPkEonj-RkWhqCzOHul7m8siUA",
          category: "Office Furniture",
        },
        {
          name: "Frontier",
          description:
            "Get the power of 100% fiber optic network for your home with fiber internet service from Frontier.",
          imageUrl:
            "https://www.democratandchronicle.com/gcdn/presto/2020/01/22/PROC/6b3636a6-b919-4fb5-9855-44fb20d3bcc4-636136744480621331-Frontier.jpg",
          category: "Internet",
        },
        {
          name: "Earthlink",
          description:
            "EarthLink is an award-winning Internet Service Provider offering reliable, high-speed internet and America's largest and best Mobile phone service.",
          imageUrl:
            "https://www.getmailbird.com/assets/imgs/logos/clearbit/earthlink.net.png",
          category: "Internet",
        },
      ],
    },

    {
      title: "Work From Home",
      merchants: [
        {
          name: "Best Buy",
          description:
            "Shop best buy for electronics, computers, cell phones and appliances",
          imageUrl:
            "https://corporate.bestbuy.com/wp-content/uploads/2018/05/2018_rebrand_blog_logo_LEAD_ART.jpg",
          category: "Tech Accessories",
        },
        {
          name: "Overstock",
          description:
            "We sell brand new bedroom, dining, office & living room furniture as well as mattresses, home goods and rugs",
          imageUrl:
            "https://yt3.googleusercontent.com/GE5v7bCQLsQI_VqjyHxCz1dz6G_h_FU9j_M_PyJvp1XMLb1xnYdUYWstARph15ti8h52KD1xng=s900-c-k-c0x00ffffff-no-rj",
          category: "Work From Home",
        },
        {
          name: "Amazon",
          description:
            "Free shipping on millions of items. Get the best of Shopping and Entertainment with Prime.",
          imageUrl:
            "https://logosmarcas.net/wp-content/uploads/2020/04/Amazon-Logo.png",
          category: "Work From Home",
        },
        {
          name: "Lamps Plus",
          description:
            "Shop 1000s of quality lamps & lighting fixtures at Lamps Plus.",
          imageUrl: "https://www.lampsplus.com/images/lampsplus-300x300.png",
          category: "Office Furniture",
        },
      ],
    },
    {
      title: "Well-being",
      merchants: [
        {
          name: "Exhale",
          description:
            "Transformational fitness classes with barre & yoga classes",
          imageUrl:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSt-HXTG4eL387F5budJENLzL3trkLxG9RdniNDT9CnN7i6e1ZisF5wd7nU5JQgSMXVHu0&usqp=CAU",
          category: "Gym",
        },
        {
          name: "Yoga Works",
          description:
            "YogaWorks provides diverse styles of yoga, fitness & teacher training classes under one umbrella.",
          imageUrl:
            "https://images.labusinessjournal.com/wp-content/uploads/2021/11/yogaworks.png",
          category: "Gym",
        },
        {
          name: "Runkeeper",
          description:
            "Just like a real-life coach, the ASICS Runkeeper app will help you set your goals, train for them, and track your progress along the way..",
          imageUrl:
            "https://play-lh.googleusercontent.com/6E8NcQQijI7tPgtkBcZ8OnES_jVsrEdLpC_mH_1kGQRAb3uSzKUp8HxiGTqnUXucaa4",
          category: "Fitness App",
        },
        {
          name: "Brightside",
          description:
            "Brightside is an online therapy and medication management program intended for adults (ages 18 and up) who are experiencing depression or anxiety",
          imageUrl:
            "https://www.brightside.com/wp-content/uploads/2023/05/social-share-banner.png",
          category: "Healthy Lifestyle",
        },
      ],
    },
  ];

  return merchantsCategories;
};
