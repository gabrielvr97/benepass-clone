import { Update } from "../models/Update";

export function getUpdates(): Update[] {
  return [
    {
      title: "Action Required",
      description: "1 transaction required your attention!",
      icon: "warning",
      color: "yellow",
    },
    {
      title: "Order a physical card",
      description: "Cards arrive in 7-10 business days!",
      icon: "card",
      color: "blue",
    },
    {
      title: "Finish account setup",
      description: "Link your bank account to submit reimbursements",
      icon: "bank",
      color: "blue",
    },
    {
      title: "New contribution!",
      description: "You received 4 new contributions from your employer!",
      icon: "party",
      color: "green",
    },
  ];
}
