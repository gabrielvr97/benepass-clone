import { Transaction } from "../models/Transaction";

export const getRecentTransactions = (): Transaction[] => {
  const recentTransactions: Transaction[] = [
    {
      id: 2345,
      establishment: "Amazon",
      category: "Work From Home",
      amount: 100.76,
      status: "PENDING",
      date: "17 Feb",
    },
    {
      id: 2346,
      establishment: "AT&T",
      category: "Cellphone + Internet",
      amount: 69.9,
      status: "ACTION_REQUIRED",
      date: "17 Feb",
    },
    {
      id: 2347,
      establishment: "Mc Donald's",
      category: "Food",
      amount: 10.5,
      status: "CANCELED",
      date: "16 Feb",
    },
    {
      id: 2348,
      establishment: "Burger King",
      category: "Food",
      amount: 12.8,
      status: "CONFIRMED",
      date: "15 Feb",
    },
    {
      id: 2349,
      establishment: "Best Buy",
      category: "Work From Home",
      amount: 230.8,
      status: "CONFIRMED",
      date: "11 Feb",
    },
  ];
  return recentTransactions;
};
