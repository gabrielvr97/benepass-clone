export interface Update {
  title: string;
  description: string;
  color: string;
  icon: string;
}
