export interface Merchant {
  name: string;
  description: string;
  imageUrl: string;
  category: string;
}

export interface MerchantCategory {
  title: string;
  merchants: Merchant[];
}
