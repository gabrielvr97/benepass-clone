export type TransactionStatus =
  | "CONFIRMED"
  | "PENDING"
  | "CANCELED"
  | "ACTION_REQUIRED";

export interface Transaction {
  id: number;
  establishment: string;
  category: string;
  amount: number;
  date: string;
  status: TransactionStatus;
}
